# Ariadne

Programa de monitoramento e download automático de conteúdo publicado em mídias sociais (Facebook, Instagram, Twitter) para fins acadêmicos.

### Como funciona?

Ariadne utiliza uma conta informada pelo operador para acessar seu perfil pessoal na rede social escolhida e simular a atividade de um usuário comum. Neste momento, o software consegue gravar em seu banco de dados o conteúdo disponível. 

### Posso baixar conteúdo que não tenho acesso em meu perfil?

Não. O acesso de Ariadne está limitado ao acesso que a conta informada possui na rede social escolhida. Isso significa que você não pode usar Ariadne para fazer download do conteúdo de perfis, páginas e grupos privados aos quais sua conta não tem acesso. Também é impossível usar Ariadne para entrar em grupos ou se comunicar com pessoas de maneira automática (o que se configuraria como SPAM), Ariadne só tem permissões de acesso, não de escrita.

### Ariadne é legal?

Tecnicamente, sim. Ariadne simula o acesso comum de leitura de um usuário de rede social, de forma que não perturba o funcionamento da rede nem se aproveita de alguma falha de segurança para ter acesso. Entretanto, o conteúdo baixado por Ariadne pertence unicamente aos usuários, páginas e grupos que o publicou, portanto, o uso desses dados deve ser feita de maneira responsável e de acordo com os termos de uso das respectivas redes sociais. 

### Para que Ariadne foi criado?

O software foi desenvolvido com foco na pesquisa acadêmica, automatizando as tarefas de coleta de dados nas mídias sociais. Não nos responsabilizamos por qualquer outro uso.
